package com.liodevel.lioapp_1.Utils;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.google.common.collect.Lists;
import com.liodevel.lioapp_1.Objects.Track;
import com.liodevel.lioapp_1.Objects.TrackPoint;
import com.liodevel.lioapp_1.R;
import com.mapbox.mapboxsdk.geometry.LatLng;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by emilio on 21/12/2015.
 */
public class Server {


    public static ArrayList<Track> getTracks(){
        Utils.logInfo("getTracks()");



        ArrayList<Track> ret = new ArrayList<>();
        Iterator<Track> tracksIt = Track.findAll(Track.class);
        ret = Lists.newArrayList(tracksIt);

        for (Track track: ret) {
            if (!track.isClosed()) {
                Utils.logInfo("Track Incomplete");
                try {
                    Server.fixTrack(track.getId());
                } catch (Exception e){
                    Utils.logError("Error FixTrack...");
                }
            }
        }

        Utils.logInfo(ret.toString());

        return ret;
    }


    public static ArrayList<Track> getFavoriteTracks(){
        Utils.logInfo("getFavoriteTracks()");

        ArrayList<Track> ret = new ArrayList<>();
        List<Track> tracksIt = Track.find(Track.class, "favorite = ?", "1");
        ret = Lists.newArrayList(tracksIt);

        for (Track track: ret) {
            if (!track.isClosed()) {
                Utils.logInfo("Track Incomplete");
                Server.fixTrack(track.getId());
            }
        }

        Utils.logInfo(ret.toString());

        return ret;

    }

    /**
     * Borrar Track
     * @param objectId
     */
    public static void deleteTrackByObjectId(long objectId) {
        Utils.logInfo("deleteTrackByObjectId()");
        Track track = Track.findById(Track.class, objectId);
        track.delete();
    }





    /**
     * Borra los Tracks seleccionados
     */
    public static void deleteSelectedTracks(ArrayList<Long> selectedTracksId, Context context) {
        Utils.logInfo("deleteSelectedTracks()");
        if (selectedTracksId.size() > 0) {

            try {
                int cont = 0;
                for (Long id : selectedTracksId) {
                    Track track = Track.findById(Track.class, id);
                    track.delete();
                    cont++;
                }
                Utils.showMessage(context, cont + " " + context.getResources().getString(R.string.tracks_deleted));
            } catch (Exception ex){
                Utils.logInfo("Error deleting: " + ex.toString());
            }
        }
    }



    /**
     * Repara los Tracks mal guardados
     * @param id Track id
     */
    public static void fixTrack(long id){

        Track currentTrack = null;
        currentTrack = Track.findById(Track.class, id);

        LatLng prevPos = null;
        LatLng actualPos = null;
        TrackPoint previousTrackPoint = new TrackPoint();

        if (currentTrack != null) {

            List<TrackPoint> trackPoints = new ArrayList<>();
            trackPoints = TrackPoint.findWithQuery(TrackPoint.class, "trackId = ?", String.valueOf(id));
            float totalDistance = 0;
            Date lastDate = null;

            for (TrackPoint trackPoint : trackPoints) {
                actualPos = new LatLng(trackPoint.getLat(), trackPoint.getLng());
                if (prevPos != null) {
                    if(previousTrackPoint != null) {
                        Location selected_location = new Location("locationA");
                        selected_location.setLatitude(trackPoint.getLat());
                        selected_location.setLongitude( trackPoint.getLng());
                        Location near_locations = new Location("locationA");
                        near_locations.setLatitude(previousTrackPoint.getLat());
                        near_locations.setLongitude(previousTrackPoint.getLng());

                        double distance = selected_location.distanceTo(near_locations);
                        totalDistance = totalDistance + ((float)distance);
                    }
                }

                prevPos = actualPos;
                previousTrackPoint = trackPoint;
            }

            currentTrack.setDistance(totalDistance);
            currentTrack.setDistance(totalDistance);
            currentTrack.setClosed(true);

            if (lastDate != null){
                currentTrack.setDateEnd(lastDate);
            }
            currentTrack.save();
            Utils.logInfo("Track Fixed! ");


        }


    }


}
