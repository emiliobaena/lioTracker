package com.liodevel.lioapp_1.Activities;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Xml;
import android.view.Menu;
import android.view.MenuItem;
import android.view.TextureView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.answers.ShareEvent;
import com.google.common.escape.Escaper;
import com.liodevel.lioapp_1.Objects.Track;
import com.liodevel.lioapp_1.Objects.TrackPoint;
import com.liodevel.lioapp_1.R;
import com.liodevel.lioapp_1.Utils.ScreenshotUtil;
import com.liodevel.lioapp_1.Utils.Server;
import com.liodevel.lioapp_1.Utils.Utils;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.constants.Style;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.views.MapView;
import com.orm.SugarContext;

import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TrackActivity extends AppCompatActivity {

    SharedPreferences prefs;
    private String systemOfMeasurement = "metric";

    private Menu actionBarMenu;
    private static Context context;
    private long trackObjectId = -1;
    private Track currentTrack = new Track();
    private static ProgressDialog progress;
    long trackPointsCount = 0;

    private MapView mapView = null;
    private String mapStyle = Style.MAPBOX_STREETS;
    private double totalDistance = 0.0;

    //private Toolbar myToolbar;
    private TextView durationInfo;
    private TextView distanceInfo;
    private TextView info;
    private TextView date;
    private TextView averageSpeed;
    private EditText editInfo;

    List<TrackPoint> trackPoints = new ArrayList<>();

    private ImageView vehicleIcon;
    private LinearLayout leyenda1, leyenda2, leyenda3, leyenda4, leyenda5, leyendaColores;
    private RelativeLayout layoutToShare;

    Icon iconTrack;
    Icon iconStart;
    Icon iconEnd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track);
        context = this;

        try {
            SugarContext.init(context);
        } catch (Exception e){
            Crashlytics.logException(e);
            Utils.logError(e.toString());
        }

        IconFactory mIconFactory = IconFactory.getInstance(this);
        Drawable mIconTrack = ContextCompat.getDrawable(this, R.drawable.ic_void);
        iconTrack = mIconFactory.fromDrawable(mIconTrack);

        Drawable mIconStart = ContextCompat.getDrawable(this, R.drawable.ic_marker_green);
        iconStart = mIconFactory.fromDrawable(mIconStart);

        Drawable mIconEnd = ContextCompat.getDrawable(this, R.drawable.ic_marker_red);
        iconEnd = mIconFactory.fromDrawable(mIconEnd);


        // VISTA
        changeNotificationBar();
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.liodevel_white)));
        getSupportActionBar().setElevation(0);

        durationInfo = (TextView) findViewById(R.id.text_track_duration_track_info);
        distanceInfo = (TextView) findViewById(R.id.text_track_distance_track_info);
        info = (TextView) findViewById(R.id.text_track_info);
        editInfo = (EditText) findViewById(R.id.edit_info);
        date = (TextView) findViewById(R.id.text_track_date);
        averageSpeed = (TextView) findViewById(R.id.text_track_average_speed_track_info);
        getSupportActionBar().setTitle("");

        leyenda1 = (LinearLayout) findViewById(R.id.track_leyenda_1);
        leyenda2 = (LinearLayout) findViewById(R.id.track_leyenda_2);
        leyenda3 = (LinearLayout) findViewById(R.id.track_leyenda_3);
        leyenda4 = (LinearLayout) findViewById(R.id.track_leyenda_4);
        leyenda5 = (LinearLayout) findViewById(R.id.track_leyenda_5);
        leyendaColores = (LinearLayout) findViewById(R.id.track_leyenda_colores);
        vehicleIcon = (ImageView) findViewById(R.id.track_vehicle_icon);

        layoutToShare = (RelativeLayout) findViewById(R.id.layout_to_share);

        mapView = (MapView) findViewById(R.id.mapbox_track);
        mapView.setAccessToken(getString(R.string.com_mapbox_mapboxsdk_accessToken));
        mapView.onCreate(savedInstanceState);

        // Shared Preferences
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        try {
            mapStyle = prefs.getString("map_style", Style.MAPBOX_STREETS);
            Utils.logInfo("-PREFS- mapStyle: " + mapStyle);

        }catch (Exception e){
            Crashlytics.logException(e);
            Utils.logError(e.toString());
        }
        if (mapStyle.equals("MINIMAL")){
            mapView.setStyleUrl("mapbox://styles/cijzk32g72r89wdki5qegzstj/cik6u7ln800g8b5m01lzvl7lt");
        } else if (mapStyle.equals("HILLSHADES_SAT")) {
            mapView.setStyleUrl("mapbox://styles/cijzk32g72r89wdki5qegzstj/cik6y8j9300h5b5m0jyc7o5oj");
        }else {
            mapView.setStyle(mapStyle);
        }
        try {
            systemOfMeasurement = prefs.getString("system_of_measurement", "metric");
            Utils.logInfo("-PREFS- SOM : " + systemOfMeasurement);

        }catch (Exception e){
            Crashlytics.logException(e);
            Utils.logError(e.toString());
        }


        //progressDialog.show(this, "Track", "Downloading track", true);
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            trackObjectId = -1;
        } else {
            trackObjectId = extras.getLong("objectId");
            Utils.logInfo("ObjectId Track: " + trackObjectId);
        }

        if (mapView == null) {
            Utils.showMessage(getApplicationContext(), getResources().getString(R.string.unable_to_create_map));
        }

        getTrackByObjectId(trackObjectId);
        updateTrackInfo();
        toggleVehicle(currentTrack.getVehicle());
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        updateTrack();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        actionBarMenu = menu;
        getMenuInflater().inflate(R.menu.menu_actionbar_track, menu);

        // Favorito
        if (currentTrack != null && currentTrack.isFavorite()){
            actionBarMenu.findItem(R.id.track_action_favorite).setIcon(R.drawable.ic_action_action_favorite);;
        } else {
            actionBarMenu.findItem(R.id.track_action_favorite).setIcon(R.drawable.ic_action_action_favorite_outline);;
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.map_action_delete_track:
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder
                        .setMessage(getResources().getString(R.string.delete_track))
                        .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                Server.deleteTrackByObjectId(trackObjectId);
                                finish();
                            }
                        })
                        .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                        .show();
                return true;

            // TIPO MAPA
            case R.id.track_action_type_map:
                toggleMapType();
                return true;

            // FAVORITO
            case R.id.track_action_favorite:
                //
                Answers.getInstance().logCustom(new CustomEvent("Toggle Favorite"));
                //
                updateTrackFavorite();
                return true;

            // EXPORTAR
            case R.id.track_action_export_kml:
                try {
                    shareKML(exportKML());
                } catch (Exception e){
                    Crashlytics.logException(e);
                    Utils.showMessage(getApplicationContext(), "Error");
                }
                return true;

            // COMPARTIR
            case R.id.track_action_share_image:
                try {
                    shareImage(getBitmapFromView(layoutToShare));
                } catch (Exception e){
                    Crashlytics.logException(e);
                    Utils.showMessage(getApplicationContext(), "Error");
                }
                return true;


            default:
                return super.onOptionsItemSelected(item);

        }
    }

    /**
     * Get currentTrack
     *
     * @param trackId
     * @return
     */
    private boolean getTrackByObjectId(long trackId) {
        Utils.logInfo("getTrackByObjectId()");

        progress = new ProgressDialog(context);
        progress.setMessage(getResources().getString(R.string.loading_track));
        progress.show();

        boolean ret = false;
        LatLng prevPos = null;
        LatLng actualPos;
        TrackPoint previousTrackPoint = new TrackPoint();

        currentTrack = Track.findById(Track.class, trackId);

        if (currentTrack.getVehicle() == 0){
            currentTrack.setVehicle(1);
        }
        Utils.logInfo("Track Vehicle: " + currentTrack.getVehicle());
        Utils.logInfo("Track ID: " + currentTrack.getId());
        ret = true;

        if (ret == true) {

            trackPoints = TrackPoint.find(TrackPoint.class, "track_id = ?", String.valueOf(trackId));

            int cont = 0;
            for (TrackPoint trackPoint: trackPoints) {
                cont++;
                Utils.logInfo("---LATLNG: " + trackPoint.getLat() + ", " + trackPoint.getLng() );
                actualPos = new LatLng(trackPoint.getLat(), trackPoint.getLng());
                //Utils.logInfo("---LATLNG: " + actualPos.getLatitude() + ", " + actualPos.getLongitude());
                if (prevPos != null) {
                    if(previousTrackPoint != null) {

                        Location selected_location = new Location("locationA");
                        selected_location.setLatitude(trackPoint.getLat());
                        selected_location.setLongitude( trackPoint.getLng());
                        Location near_locations = new Location("locationA");
                        near_locations.setLatitude(previousTrackPoint.getLat());
                        near_locations.setLongitude(previousTrackPoint.getLng());

                        double distance = selected_location.distanceTo(near_locations);
                        double kilometers;
                        if (systemOfMeasurement.equals("metric")) {
                            kilometers = distance / 1000.0;
                        }else {
                            kilometers = distance / 1609.3;
                        }
                        //Utils.logInfo("TRACKPOINT DISTANCE      :" + distance);
                        //Utils.logInfo("TRACKPOINT DATE          :" + trackPoint.getDate().getTime());
                        //Utils.logInfo("PREVIOUSTRACKPOINT DATE  :" + previousTrackPoint.getDate().getTime());
                        long microsecs = (trackPoint.getDate().getTime() - previousTrackPoint.getDate().getTime());
                        double hours = microsecs / 1000.0 / 3600.0;
                        double speed = kilometers / hours;
                        //Utils.logInfo("" + speed + "km/h");

                        totalDistance = totalDistance + kilometers;
                        trackPoint.setSpeed(speed);
                        drawTrackPoint(prevPos, actualPos, speed, currentTrack.getVehicle(), cont, totalDistance);
                    }
                } else {
                    // Centrar en primera localización
                    if (mapView != null){
                        Utils.logInfo("---LATLNG: " + actualPos.getLatitude() + ", " + actualPos.getLongitude());

                        mapView.setCenterCoordinate(actualPos);
                        mapView.setZoom(12);
                        mapView.setTilt(75.0, (long) 4000);

                        String hora = trackPoint.getDate().toString();
                        try {
                            hora = trackPoint.getDate().toString().substring(11,19);
                        } catch (Exception e){
                            Crashlytics.logException(e);
                            hora = trackPoint.getDate().toString();
                        }
                        MarkerOptions markerOptions;
                        Marker marker;
                        markerOptions = new MarkerOptions()
                                .position(new LatLng(actualPos.getLatitude(), actualPos.getLongitude()))
                                .icon(iconStart)
                                .title(getResources().getString(R.string.track_start) + " (" + hora + ")")
                                .snippet(actualPos.getLatitude() + ", " + actualPos.getLongitude());
                        marker = mapView.addMarker(markerOptions);
                    }
                }

                prevPos = actualPos;
                previousTrackPoint = trackPoint;
                //trackPoints.add(trackPoint);
            }

            MarkerOptions markerOptions;
            Marker marker;
            String hora = previousTrackPoint.getDate().toString();
            try {
                hora = previousTrackPoint.getDate().toString().substring(11,19);
            } catch (Exception e){
                Crashlytics.logException(e);
                hora = previousTrackPoint.getDate().toString();
            }
            markerOptions = new MarkerOptions()
                    .position(new LatLng(prevPos.getLatitude(), prevPos.getLongitude()))
                    .icon(iconEnd)
                    .title(getResources().getString(R.string.track_end) + " (" + hora + ")")
                    .snippet(prevPos.getLatitude() + ", " + prevPos.getLongitude());

            //.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_green));
            marker = mapView.addMarker(markerOptions);
            Utils.logInfo("TOTAL TrackPoints: " + cont);
            ret = true;

        }
        progress.dismiss();
        return ret;
    }



    /**
     *
     */
    private void updateTrackInfo() {
        Date currentDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(currentTrack.getDate());

        // Info
        if (currentTrack.getInfo() != null && currentTrack.getInfo().length() > 0) {
            info.setText(currentTrack.getInfo());
            editInfo.setText(currentTrack.getInfo());
        } else {
            info.setText(getResources().getString(R.string.insert_track_info));
            editInfo.setText(getResources().getString(R.string.insert_track_info));
            info.setTextColor(ContextCompat.getColor(this, R.color.liodevel_grey));
        }

        date.setText(currentTrack.getDate().toString());

        // Date
        if (currentDate.getTime() - currentTrack.getDate().getTime() < TimeUnit.MILLISECONDS.convert(6, TimeUnit.DAYS)) {
            String weekDay = "";
            if (c.get(Calendar.DAY_OF_WEEK) == 1) {
                weekDay = this.getResources().getString(R.string.sunday);
            } else if (c.get(Calendar.DAY_OF_WEEK) == 2) {
                weekDay = this.getResources().getString(R.string.monday);
            } else if (c.get(Calendar.DAY_OF_WEEK) == 3) {
                weekDay = this.getResources().getString(R.string.tuesday);
            } else if (c.get(Calendar.DAY_OF_WEEK) == 4) {
                weekDay = this.getResources().getString(R.string.wednesday);
            } else if (c.get(Calendar.DAY_OF_WEEK) == 5) {
                weekDay = this.getResources().getString(R.string.thursday);
            } else if (c.get(Calendar.DAY_OF_WEEK) == 6) {
                weekDay = this.getResources().getString(R.string.friday);
            } else if (c.get(Calendar.DAY_OF_WEEK) == 7) {
                weekDay = this.getResources().getString(R.string.saturday);
            }

            //getActionBar().setTitle(new SimpleDateFormat("HH:mm").format(currentTrack.getDate()) + "   " + weekDay);
        } else {
            //getActionBar().setTitle(new SimpleDateFormat("HH:mm").format(currentTrack.getDate()) + "   " + new SimpleDateFormat("MM-dd-yyyy").format(currentTrack.getDate()));
        }

        // Distancia
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);


        if (systemOfMeasurement.equals("metric")) {
            if (currentTrack.getDistance() < 1000) {
                distanceInfo.setText(df.format(currentTrack.getDistance()) + " m");
            } else {
                distanceInfo.setText(df.format((currentTrack.getDistance() / 1000)) + " km");
            }
        } else {
            if (currentTrack.getDistance() < 1000) {
                distanceInfo.setText(df.format(currentTrack.getDistance() / 0.9144) + " yd");
            } else {
                distanceInfo.setText(df.format((currentTrack.getDistance() / 1609.3)) + " mi");
            }
        }

        // Duration
        double durationDouble = 0.0;
        if (currentTrack.getDateEnd() != null) {
            Long durationLong = currentTrack.getDateEnd().getTime() - currentTrack.getDate().getTime();
            // duracion en minutos;
            durationDouble = durationLong / 1000 / 60;
            durationInfo.setText(Utils.minutesToHour(durationDouble));

        } else {
            durationInfo.setText("");
        }

        // Velocidad media
        double averageSpeedFloat = 0.0f;
        // METRICO
        if (systemOfMeasurement.equals("metric")) {
            averageSpeedFloat = (currentTrack.getDistance() / 1000.0f) / (durationDouble / 60.0);
            averageSpeed.setText(df.format(averageSpeedFloat) + " km/h");
        } else {
            averageSpeedFloat = (currentTrack.getDistance() / 1609.3f) / (durationDouble / 60.0);
            averageSpeed.setText(df.format(averageSpeedFloat) + " mi/h");
        }


    }

    /**
     * Oculta Info y muestra el EditInfo
     * @param v
     */
    public void toggleVisibilityInfo(View v) {
        editInfo.setVisibility(View.VISIBLE);
        info.setVisibility(View.INVISIBLE);
    }

    /**
     * Actualiza campo info en la Base de datos
     */
    private void updateTrack() {

        if (!editInfo.getText().toString().equals(info.getText().toString())) {
            currentTrack.setInfo(editInfo.getText().toString());
            currentTrack.save();
        }
    }


    /**
     * Actualiza campo info en la Base de datos
     */
    private void updateTrackFavorite() {

        if (currentTrack.isFavorite()) {
            currentTrack.setFavorite(false);
            actionBarMenu.findItem(R.id.track_action_favorite).setIcon(R.drawable.ic_action_action_favorite_outline);;
        } else {
            currentTrack.setFavorite(true);
            actionBarMenu.findItem(R.id.track_action_favorite).setIcon(R.drawable.ic_action_action_favorite);;
        }

        currentTrack.save();

    }


    /**
     * Cambiar tipo de mapa
     */
    private void toggleMapType(){

        if (mapStyle.equals("HILLSHADES_SAT")){
            mapView.setStyle(Style.DARK);
            mapStyle = Style.DARK;

        } else if (mapStyle.equals(Style.DARK)){
            mapView.setStyle(Style.EMERALD);
            mapStyle = Style.EMERALD;

        } else if (mapStyle.equals(Style.EMERALD)){
            mapView.setStyle(Style.LIGHT);
            mapStyle = Style.LIGHT;

        } else if (mapStyle.equals(Style.LIGHT)){
            mapView.setStyle(Style.SATELLITE_STREETS);
            mapStyle = Style.SATELLITE_STREETS;

        } else if (mapStyle.equals(Style.SATELLITE_STREETS)){
            mapView.setStyle(Style.MAPBOX_STREETS);
            mapStyle = Style.MAPBOX_STREETS;

        } else if (mapStyle.equals(Style.MAPBOX_STREETS)){
            mapView.setStyleUrl("mapbox://styles/cijzk32g72r89wdki5qegzstj/cik6u7ln800g8b5m01lzvl7lt");
            mapStyle = "MINIMAL";
        } else {
            mapView.setStyleUrl("mapbox://styles/cijzk32g72r89wdki5qegzstj/cik6y8j9300h5b5m0jyc7o5oj");
            mapStyle = "HILLSHADES_SAT";
        }

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("map_style", mapStyle);
        Utils.logInfo("---map_style: " + mapStyle);
        editor.apply();

    }



    /**
     * Dibuja una linea en el mapa
     * @param start Coordenadas inicio
     * @param end Coordenadas final
     * @param speed velocidad en KM/H
     * @param vehicle 1-Coche; 2-Moto; 3-Bici; 4-Andando; 5-Corriendo
     */
    private void drawTrackPoint(LatLng start, LatLng end, double speed, int vehicle, int counter, double distance) {
        int colorTrack;
        Utils.logInfo("Drawing");

        // Coche o Moto
        if (vehicle == 1 || vehicle == 2) {
            if (speed < 10) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_black);
            } else if (speed < 20) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_red);
            } else if (speed < 30) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_orange);
            } else if (speed < 40) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_yellow);
            } else if (speed < 50) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_green);
            } else if (speed < 70) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_dark_green);
            } else if (speed < 90) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_blue);
            } else if (speed < 120) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_cyan);
            } else {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_magenta);
            }

            // Bici
        } else if (vehicle == 3) {
            if (speed < 5) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_black);
            } else if (speed < 10) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_red);
            } else if (speed < 15) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_orange);
            } else if (speed < 20) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_yellow);
            } else if (speed < 25) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_green);
            } else if (speed < 35) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_dark_green);
            } else if (speed < 45) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_blue);
            } else if (speed < 55) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_cyan);
            } else {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_magenta);
            }
            // Andando
        } else if (vehicle == 4) {
            if (speed < 2) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_black);
            } else if (speed < 4) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_red);
            } else if (speed < 6) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_orange);
            } else if (speed < 8) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_yellow);
            } else if (speed < 10) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_green);
            } else if (speed < 12) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_dark_green);
            } else if (speed < 14) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_blue);
            } else if (speed < 16) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_cyan);
            } else {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_magenta);
            }

            // Corriendo
        } else {
            if (speed < 5) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_black);
            } else if (speed < 10) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_red);
            } else if (speed < 15) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_orange);
            } else if (speed < 20) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_yellow);
            } else if (speed < 25) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_green);
            } else if (speed < 30) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_dark_green);
            } else if (speed < 35) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_blue);
            } else if (speed < 40) {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_cyan);
            } else {
                colorTrack = ContextCompat.getColor(this, R.color.liodevel_chart_magenta);
            }
        }

        String units = "km";
        if (systemOfMeasurement.equals("metric")){
            units = "km";
        } else {
            units = "mi";
        }

        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);

        if (mapView != null) {
            PolylineOptions line =
                    new PolylineOptions().add(start, end)
                            .width(8).color(colorTrack);
            mapView.addPolyline(line);

            MarkerOptions markerOptions;
            Marker marker;
            markerOptions = new MarkerOptions()
                    .position(end)
                    .icon(iconTrack)
                    .title(getResources().getString(R.string.point) + " " + counter)
                    .snippet(end.getLatitude() + ", " + end.getLongitude() + "\n"
                    + getResources().getString(R.string.distance_from_start) + ": " + df.format(distance) + units);
            marker = mapView.addMarker(markerOptions);

        }
    }


    private File exportKML() {

        Utils.logInfo("exportKML()");
        XmlSerializer xmlSerializer;

        //File newxmlfile = new File("/sdcard/new.kml");
        File newxmlfile = new File(Utils.getAppFolder() + currentTrack.getDate().toString().replace(" ", "_").replace(":","_") + ".kml");
        //Utils.logInfo("PATH: " + getApplicationInfo().dataDir + "/new.xml");

        try{
            newxmlfile.createNewFile();
        }catch(IOException e){
            Crashlytics.logException(e);
            Utils.logError("IOException: Exception in create new File(");
        }

        FileOutputStream fileos = null;
        try{
            fileos = new FileOutputStream(newxmlfile);

        }catch(FileNotFoundException e){
            Crashlytics.logException(e);
            Log.e("FileNotFoundException",e.toString());
        }

        try {

            xmlSerializer = Xml.newSerializer();

            xmlSerializer.setOutput(fileos, "UTF-8");
            xmlSerializer.startDocument("UTF-8", true);

            //xmlSerializer.setFeature( "http://xmlpull.org/v1/doc/features.html#indent-output", true);

            xmlSerializer.startTag(null, "kml");
            xmlSerializer.attribute(null, "xmlns", "http://earth.google.com/kml/2.2");
            xmlSerializer.startTag(null, "Document");

            xmlSerializer.startTag(null, "name");
            xmlSerializer.text(currentTrack.getDate().toString() + " - " + currentTrack.getInfo());
            xmlSerializer.endTag(null, "name");

            xmlSerializer.startTag(null, "description");
            xmlSerializer.cdsect("Your personal GPS tracker");
            xmlSerializer.endTag(null, "description");

            // STYLES

            // Style NEGRO
            xmlSerializer.startTag(null, "Style");
            xmlSerializer.attribute(null, "id", "black");
            xmlSerializer.startTag(null, "LineStyle");
            xmlSerializer.startTag(null, "color");
            xmlSerializer.text(Utils.BLACK);
            xmlSerializer.endTag(null, "color");
            xmlSerializer.startTag(null, "width");
            xmlSerializer.text("4");
            xmlSerializer.endTag(null, "width");
            xmlSerializer.endTag(null, "LineStyle");
            xmlSerializer.endTag(null, "Style");

            // Style ROJO
            xmlSerializer.startTag(null, "Style");
            xmlSerializer.attribute(null, "id", "red");
            xmlSerializer.startTag(null, "LineStyle");
            xmlSerializer.startTag(null, "color");
            xmlSerializer.text(Utils.RED);
            xmlSerializer.endTag(null, "color");
            xmlSerializer.startTag(null, "width");
            xmlSerializer.text("4");
            xmlSerializer.endTag(null, "width");
            xmlSerializer.endTag(null, "LineStyle");
            xmlSerializer.endTag(null, "Style");

            // Style AMARILLO
            xmlSerializer.startTag(null, "Style");
            xmlSerializer.attribute(null, "id", "yellow");
            xmlSerializer.startTag(null, "LineStyle");
            xmlSerializer.startTag(null, "color");
            xmlSerializer.text(Utils.YELLOW);
            xmlSerializer.endTag(null, "color");
            xmlSerializer.startTag(null, "width");
            xmlSerializer.text("4");
            xmlSerializer.endTag(null, "width");
            xmlSerializer.endTag(null, "LineStyle");
            xmlSerializer.endTag(null, "Style");

            // Style VERDE
            xmlSerializer.startTag(null, "Style");
            xmlSerializer.attribute(null, "id", "green");
            xmlSerializer.startTag(null, "LineStyle");
            xmlSerializer.startTag(null, "color");
            xmlSerializer.text(Utils.GREEN);
            xmlSerializer.endTag(null, "color");
            xmlSerializer.startTag(null, "width");
            xmlSerializer.text("4");
            xmlSerializer.endTag(null, "width");
            xmlSerializer.endTag(null, "LineStyle");
            xmlSerializer.endTag(null, "Style");

            // Style CYAN
            xmlSerializer.startTag(null, "Style");
            xmlSerializer.attribute(null, "id", "cyan");
            xmlSerializer.startTag(null, "LineStyle");
            xmlSerializer.startTag(null, "color");
            xmlSerializer.text(Utils.CYAN);
            xmlSerializer.endTag(null, "color");
            xmlSerializer.startTag(null, "width");
            xmlSerializer.text("4");
            xmlSerializer.endTag(null, "width");
            xmlSerializer.endTag(null, "LineStyle");
            xmlSerializer.endTag(null, "Style");

            // Style AZUL
            xmlSerializer.startTag(null, "Style");
            xmlSerializer.attribute(null, "id", "blue");
            xmlSerializer.startTag(null, "LineStyle");
            xmlSerializer.startTag(null, "color");
            xmlSerializer.text(Utils.BLUE);
            xmlSerializer.endTag(null, "color");
            xmlSerializer.startTag(null, "width");
            xmlSerializer.text("4");
            xmlSerializer.endTag(null, "width");
            xmlSerializer.endTag(null, "LineStyle");
            xmlSerializer.endTag(null, "Style");

            // Style MAGENTA
            xmlSerializer.startTag(null, "Style");
            xmlSerializer.attribute(null, "id", "magenta");
            xmlSerializer.startTag(null, "LineStyle");
            xmlSerializer.startTag(null, "color");
            xmlSerializer.text(Utils.MAGENTA);
            xmlSerializer.endTag(null, "color");
            xmlSerializer.startTag(null, "width");
            xmlSerializer.text("4");
            xmlSerializer.endTag(null, "width");
            xmlSerializer.endTag(null, "LineStyle");
            xmlSerializer.endTag(null, "Style");


            // Placemark

                xmlSerializer.startTag(null, "Placemark");
                xmlSerializer.startTag(null, "styleUrl");
                xmlSerializer.text("#red");
                xmlSerializer.endTag(null, "styleUrl");
                /*
                xmlSerializer.startTag(null, "name");

                if (currentTrack.getInfo() != null && currentTrack.getInfo().length() > 0){
                    xmlSerializer.text(currentTrack.getInfo());
                } else {
                    xmlSerializer.text("My Tracker");
                }
                xmlSerializer.endTag(null, "name");
                xmlSerializer.startTag(null, "description");
                xmlSerializer.cdsect("");
                xmlSerializer.endTag(null, "description");
                */


                xmlSerializer.startTag(null, "LineString");

                xmlSerializer.startTag(null, "coordinates");
            for (TrackPoint tr:trackPoints) {

                // Insertar Coordenadas
                xmlSerializer.text(Double.toString(tr.getLng()) + "," + Double.toString(tr.getLat()) + "\n");
            }


            xmlSerializer.endTag(null, "coordinates");
            xmlSerializer.endTag(null, "LineString");

            xmlSerializer.endTag(null, "Placemark");
            xmlSerializer.endTag(null, "Document");
            xmlSerializer.endTag(null, "kml");
            xmlSerializer.endDocument();
            xmlSerializer.flush();
            fileos.close();

            //fileos.close();
            //Utils.logInfo("FILE KML:" + writer.toString());

        } catch (Exception e) {
            Crashlytics.logException(e);
            e.printStackTrace();
            // Log.e("Exception", "Exception occured in writing");
        }

        return newxmlfile;
    }


    private void shareKML(File file) {

        Answers.getInstance().logShare(new ShareEvent().putCustomAttribute("Screen", "KML"));

        try {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            //shareIntent.setType("application/xml");
            shareIntent.setType("application/vnd.google-earth.kml+xml");
            shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            startActivity(Intent.createChooser(shareIntent, getResources().getText(R.string.send_to)));

        } catch (Exception e){
            Crashlytics.logException(e);
            Utils.showMessage(getApplicationContext(), "Error");
            Utils.logError(e.toString());
        }
    }


    /**
     * Share a map image
     * @param file
     */
    private void shareImage(File file) {

        Answers.getInstance().logShare(new ShareEvent().putCustomAttribute("Screen", "Track"));

        try {

            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            //shareIntent.setType("application/xml");
            shareIntent.setType("image/jpeg");
            shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
            shareIntent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.this_is_my_track));
            shareIntent.putExtra(Intent.EXTRA_TITLE, getResources().getString(R.string.app_hashtag));
            startActivity(Intent.createChooser(shareIntent, getResources().getText(R.string.send_to)));


        } catch (Exception e){
            Crashlytics.logException(e);
            Utils.showMessage(getApplicationContext(), "Error");
            Utils.logError(e.toString());
        }


    }

    /**
     *
     * @param view
     * @return
     */
    public File getBitmapFromView(View view) {

        view.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);
        Canvas canvas = new Canvas(bitmap);

        // Add the SurfaceView bit (see getAllTextureViews() below)
        List<TextureView> tilingViews = ScreenshotUtil.getAllTextureViews(view);
        if (tilingViews.size() > 0) {
            for (TextureView TextureView : tilingViews) {
                Bitmap b = TextureView.getBitmap(TextureView.getWidth(), TextureView.getHeight());
                int[] location = new int[2];
                TextureView.getLocationInWindow(location);
                int[] location2 = new int[2];
                TextureView.getLocationOnScreen(location2);
                canvas.drawBitmap(b, location[0], Utils.dpToPx(22, context), null);
            }
        }

        canvas.drawBitmap(
                BitmapFactory.decodeResource(getResources(), (R.mipmap.ic_launcher)),
                0, Utils.dpToPx(5, context),
                null);

        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setAlpha(255);
        paint.setTextSize(Utils.dpToPx(22, context));
        paint.setAntiAlias(true);
        canvas.drawText("My Tracker", Utils.dpToPx(54, context), Utils.dpToPx(43, context), paint);

        Paint paint2 = new Paint();
        paint2.setColor(Color.BLACK);
        paint2.setAlpha(255);
        paint2.setTextSize(Utils.dpToPx(22, context));
        paint2.setAntiAlias(true);
        canvas.drawText("My Tracker", Utils.dpToPx(53, context), Utils.dpToPx(42, context), paint2);


        Paint paintSub = new Paint();
        paintSub.setColor(Color.WHITE);
        paintSub.setAlpha(255);
        paintSub.setTextSize(Utils.dpToPx(16, context));
        paintSub.setAntiAlias(true);
        canvas.drawText("for Android", Utils.dpToPx(54, context), Utils.dpToPx(63, context), paintSub);

        Paint paintSub2 = new Paint();
        paintSub2.setColor(Color.BLACK);
        paintSub2.setAlpha(255);
        paintSub2.setTextSize(Utils.dpToPx(16, context));
        paintSub2.setAntiAlias(true);
        canvas.drawText("for Android", Utils.dpToPx(53, context), Utils.dpToPx(62, context), paintSub2);



        File file = new File(Utils.getAppFolder() + currentTrack.getDate().toString().replace(" ", "_").replace(":","_") + ".png");

        try {
            FileOutputStream outputStream = new FileOutputStream(file);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();


        } catch (Exception e){
            Crashlytics.logException(e);
        }

        return file;
    }




    private String getStyle(double speed){
        String ret;

        if (speed < 10) {
            ret = "#black";
        } else if (speed < 20) {
            ret = "#red";
        } else if (speed < 30) {
            ret = "#yellow";
        } else if (speed < 60) {
            ret = "#green";
        } else if (speed < 90) {
            ret = "#cyan";
        } else if (speed < 120) {
            ret = "#blue";
        } else {
            ret = "#magenta";
        }

        return ret;
    }



    private void toggleVehicle(int vehicle){

        if (vehicle == 2){
            Utils.logInfo("Vehicle -> 2");
           vehicleIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_motorcycle_black_36dp));
            leyenda2.bringToFront();
            leyendaColores.bringToFront();
        } else if(vehicle == 3){
            Utils.logInfo("Vehicle -> 3");
            vehicleIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_directions_bike_black_36dp));
            leyenda3.bringToFront();
            leyendaColores.bringToFront();

        } else if(vehicle == 4){
            Utils.logInfo("Vehicle -> 4");
            vehicleIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_directions_walk_black_36dp));
            leyenda4.bringToFront();
            leyendaColores.bringToFront();

        } else if(vehicle == 5){
            Utils.logInfo("Vehicle -> 5");
            vehicleIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_directions_run_black_36dp));
            leyenda5.bringToFront();
            leyendaColores.bringToFront();

        } else if(vehicle == 1){
            Utils.logInfo("Vehicle -> 1");
            vehicleIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_directions_car_black_36dp));
            leyenda1.bringToFront();
            leyendaColores.bringToFront();
        }
    }



    @TargetApi(21)
    private void changeNotificationBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Utils.logInfo("Notif.Bar.Coloured");
            Window window = this.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(this.getResources().getColor(R.color.liodevel_dark_green));
        } else {
            Utils.logInfo("Ap");
        }
    }

}
