package com.liodevel.lioapp_1.Activities;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;

import com.crashlytics.android.Crashlytics;
import com.liodevel.lioapp_1.Objects.Track;
import com.liodevel.lioapp_1.R;
import com.liodevel.lioapp_1.Adapters.MyTracksListAdapter;
import com.liodevel.lioapp_1.Utils.Server;
import com.liodevel.lioapp_1.Utils.Utils;
import com.orm.SugarContext;


import java.util.ArrayList;

/**
 * Pantalla My tracks
 * Con la lista de tracks guardadas
 */
public class MyTracksActivity extends AppCompatActivity {

    private static ArrayList<Track> tracks;
    private Menu actionBarMenu;
    private ListView tracksList;
    private static MyTracksListAdapter adapter;
    private static Context context;
    private int selectedPosition = -1;
    private ArrayList<Long> selectedTracksId = new ArrayList<>();
    private static ProgressDialog progress;
    private boolean selecting = false;
    private boolean favorites = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_tracks);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.liodevel_white)));
        changeNotificationBar();

        try {
            SugarContext.init(context);
        } catch (Exception e){
            Crashlytics.logException(e);
            Utils.logError(e.toString());
        }

        context = this;
        tracks = new ArrayList<>();

        // Comprobar si es pantalla de favoritos
        if (getIntent().getStringExtra("favorites") != null && getIntent().getStringExtra("favorites").equals("1")){
            favorites = true;
            getSupportActionBar().setTitle(R.string.my_favorite_tracks);
        }

        // Lista de tracks
        tracksList = (ListView) findViewById(R.id.tracks_list);

        tracksList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (!selecting) {

                    Utils.logInfo("Track selected: " + tracks.get(position).getId());
                    progress = new ProgressDialog(context);
                    progress.setMessage(getResources().getString(R.string.loading_track));
                    progress.show();

                    Intent launchNextActivity;
                    launchNextActivity = new Intent(MyTracksActivity.this, TrackActivity.class);
                    launchNextActivity.putExtra("objectId", tracks.get(position).getId());

                    startActivity(launchNextActivity);
                } else {
                    if (!selectedTracksId.contains(tracks.get(position).getId())) {
                        Utils.logInfo("SELECT");
                        //view.setBackground(getResources().getDrawable(R.drawable.item_selected));
                        tracks.get(position).setIsChecked(true);
                        actionBarMenu.findItem(R.id.map_action_delete_my_tracks).setVisible(true);
                        selecting = true;
                        selectedTracksId.add(tracks.get(position).getId());
                        Utils.logInfo("SELECTED tracks: " + selectedTracksId.toString());
                    } else {
                        Utils.logInfo("DESELECT");
                        //view.setBackground(getResources().getDrawable(R.drawable.item));
                        tracks.get(position).setIsChecked(false);
                        selectedTracksId.remove(tracks.get(position).getId());
                        Utils.logInfo("SELECTED tracks: " + selectedTracksId.toString());
                        // Si no hay nada seleccionado esconder Trash
                        if (selectedTracksId.size() == 0) {
                            actionBarMenu.findItem(R.id.map_action_delete_my_tracks).setVisible(false);
                            selecting = false;
                        }
                    }
                    Utils.logInfo("LONG CLICK: ObjectID: " + tracks.get(position).getId());
                    adapter.notifyDataSetChanged();

                }
            }
        });
        tracksList.setLongClickable(true);
        tracksList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        tracksList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                if (!selectedTracksId.contains(tracks.get(position).getId())) {
                    Utils.logInfo("SELECT");
                    //view.setBackground(getResources().getDrawable(R.drawable.item_selected));
                    tracks.get(position).setIsChecked(true);
                    actionBarMenu.findItem(R.id.map_action_delete_my_tracks).setVisible(true);
                    selecting = true;
                    selectedTracksId.add(tracks.get(position).getId());
                    Utils.logInfo("SELECTED tracks: " + selectedTracksId.toString());
                } else {
                    Utils.logInfo("DESELECT");
                    //view.setBackground(getResources().getDrawable(R.drawable.item));
                    tracks.get(position).setIsChecked(false);
                    selectedTracksId.remove(tracks.get(position).getId());
                    Utils.logInfo("SELECTED tracks: " + selectedTracksId.toString());
                    // Si no hay nada seleccionado esconder Trash
                    if (selectedTracksId.size() == 0) {
                        actionBarMenu.findItem(R.id.map_action_delete_my_tracks).setVisible(false);
                        selecting = false;
                    }
                }
                Utils.logInfo("LONG CLICK: ObjectID: " + tracks.get(position).getId());
                adapter.notifyDataSetChanged();
                return true;
            }
        });

        if (!favorites){
            tracks = Server.getTracks();
        } else {
            tracks = Server.getFavoriteTracks();
        }


        adapter = new MyTracksListAdapter(this, tracks);
        adapter.notifyDataSetChanged();
        adapter = new MyTracksListAdapter(context, tracks);
        tracksList.setAdapter(adapter);

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        adapter.clear();
        try {
            progress.dismiss();
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
        if (!favorites){
            tracks = Server.getTracks();
        } else {
            tracks = Server.getFavoriteTracks();
        }
        adapter = new MyTracksListAdapter(context, tracks);
        tracksList.setAdapter(adapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.clear();
        try {
            progress.dismiss();
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
        if (!favorites){
            tracks = Server.getTracks();
        } else {
            tracks = Server.getFavoriteTracks();
        }
        adapter = new MyTracksListAdapter(context, tracks);
        tracksList.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        actionBarMenu = menu;
        getMenuInflater().inflate(R.menu.menu_actionbar_my_tracks, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.map_action_delete_my_tracks:
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder
                        .setMessage(getResources().getString(R.string.delete_selected_tracks))
                        .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                actionBarMenu.findItem(R.id.map_action_delete_my_tracks).setVisible(false);
                                Server.deleteSelectedTracks(selectedTracksId, context);
                                adapter.clear();
                                selecting = false;
                                if (!favorites){
                                    tracks = Server.getTracks();
                                } else {
                                    tracks = Server.getFavoriteTracks();
                                }
                                adapter = new MyTracksListAdapter(context, tracks);
                                tracksList.setAdapter(adapter);

                            }
                        })
                        .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                        .show();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }




    @TargetApi(21)
    private void changeNotificationBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Utils.logInfo("Notif.Bar.Coloured");
            Window window = this.getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(this.getResources().getColor(R.color.liodevel_dark_green));
        } else {
            Utils.logInfo("Ap");
        }
    }

}





